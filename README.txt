CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Maintainers


INTRODUCTION
------------
The Calculated Rate field came out of a need to be able to setup complex rates with conditions
for several projects from Drupal UI.  This will allow you to create a field that you can then
leverage Token and create a formula for producing a value.

In addition to building a formula it includes the ability to set conditions for when that
Rate should be calculated.  If the conditions do not pass, then the value returned is 0.

REQUIREMENTS
------------
This module requires the following modules:

 * Fields API from Drupal core
 * Tokens (https://drupal.org/project/tokens)

 INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

   MAINTAINERS
   -----------

   Current maintainers:
   * Chris McIntosh (cmcintosh) - https://drupal.org/u/cmcintosh
