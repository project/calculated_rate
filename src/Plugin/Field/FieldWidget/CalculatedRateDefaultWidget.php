<?php

namespace Drupal\calculated_rate\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\calculated_rate\Plugin\Field\FieldType\CalculatedRateItem;
use Drupal\calculated_rate\Plugin\Field\FieldType\CalculatedRateItemInterface;
use Drupal\calculated_rate\Plugin\Field\FieldWidget\CalculatedRateWidgetBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'calculated_rate_default' widget.
 *
 * @FieldWidget(
 *   id = "calculated_rate_default",
 *   label = @Translation("Calculated Rate"),
 *   field_types = {
 *     "calculated_rate"
 *   }
 * )
 */
class CalculatedRateDefaultWidget extends CalculatedRateWidgetBase implements ContainerFactoryPluginInterface {
   // @todo: Still need to validate this is working, will need to do it after we also create the FieldFormatters for the Calculated Rate field.
}
