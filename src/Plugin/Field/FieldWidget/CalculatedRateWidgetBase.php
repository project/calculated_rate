<?php

namespace Drupal\calculated_rate\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\calculated_rate\Plugin\Field\FieldType\CalculatedRateItem;
use Drupal\calculated_rate\Plugin\Field\FieldType\CalculatedRateItemInterface;


/**
* Base class for the 'calculated_rate' widgets.
*/
class CalculatedRateWidgetBase extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['formula'] = [
      '#type' => 'textarea',
      '#default_value' => NULL,
      '#required' => $element['#required']
    ];

    $element['conditions'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Conditions'),
      '#description' => t('These conditions will determine if your rate is calculated. If all conditions are met the rate is calculated.  If not then a 0 value is returned.')
    ];

    $element['conditions'][0] = $this->getConditionForm($items, $delta, $element, $form, $form_state, 0);

    return $element;
  }


  private function getConditionForm(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state, $conditionDelta = 0){
    $condition['element'] = [
      '#type' => 'textfield',
      '#title' => t('Element'),
      '#description' => t('Use a token from the list to create a condition.')
    ];

    $condition['op'] = [
      '#type'  => 'select',
      '#title' => t('Operation'),
      '#description' => t('Select an evaluation operation.'),
      '#options' => [
        '==' => t('Equal'),
        '!=' => t('Not Equal'),
        '===' => t('Identical'),
        '!==' => t('Not Identical'),
        '>' => t('Greater than'),
        '<' => t('Lesser than'),
        '>=' => t('Greater than or Equal to'),
        '<=' => t('Lesser than or Equal to'),
      ]
    ];

    $condition['value'] = [
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#description' => t('Enter a token, numeric value, or string you wish to compare to the element.')
    ];

    $condition['value1'] = [
      '#type' => 'textfield',
      '#title' => t('Value 1'),
      '#description' => t('Enter a token, numeric value, or string you wish to compare to the element.')
    ];

    return $condition;
  }

}
