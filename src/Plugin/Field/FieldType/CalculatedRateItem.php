<?php

namespace Drupal\calculated_rate\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemBase;

/**
*
* Plugin implementation of the 'calculated_rate' field type.
*
* @FieldType(
*   id = "calculated_rate",
*   label = @Translation("Calculated Rate"),
*   description = @Translation("Create and store Calculated Rate values."),
*   default_widget = "calculated_rate_default",
*   default_formatter = "calculated_rate_default",
*   list_class = "\Drupal\calculated_rate\Plugin\Field\FieldType\CalculatedRateFieldItemList",
* )
*/
class CalculatedRateItem extends FieldItemBase implements CalculatedRateItemInterface {

  /**
  * {@inheritdoc}
  */
  public static function defaultStorageSettings() {
    return [
      'rate' => '',
      'conditions' => [],
    ] + parent::defaultStorageSettings();
  }

  /**
  * {@inheritdoc}
  */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['formula'] = DataDefinition::create('any')
      ->setLabel(t('Rate'))
      ->setRequired(TRUE);

    $properties['conditions'] = DataDefinition::create('map')
      ->setLabel(t('Conditions'))
      ->setRequired(TRUE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'formula' => [
          'description' => 'The rate value for this field.',
          'type' => 'text',
          'size' => 'big',
        ],
        'conditions' => [
          'description' => 'The rate value for this field.',
          'type' => 'text',
          'size' => 'big',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = [];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $type = $field_definition->getSetting('calculated_rate');
    $values['formula'] = "10 * 20" ;
    $values['conditions'] = [
      [
        'condition' => 'TRUE'
      ]
    ];
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('rate')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {
    parent::onChange($property_name, $notify);
  }

  /**
  * {@inheritdoc}
  */
  public function getValue() {
    $values = parent::getValue();
    // @TODO: add a parser to parse the formula.
    if ($this->conditionsPass($values['conditions'])) {
      $token = \Drupal::token();
      // Get the entity this field is attached to.
      $entity = $this->getEntity();
      $entity_type = $entity->getEntityTypeId();
      $formula = $token->replace($values['formula'], [ $entity_type => $entity ]);
      try {
        $values['value'] = RateParser::Execute($formula);
      }
      catch (\Exception $e) {
        watchdog_exception('calculated_rate', $e);
        throw $e;
      }
    }
    else {
      $values['value'] = 0;
    }
    return $values;
  }

  /**
  * Checks if the conditions for the rate passes.
  */
  private function conditionsPass(array $conditions) {
    $token = \Drupal::token();
    // Get the entity this field is attached to.
    $entity = $this->getEntity();
    $entity_type = $entity->getEntityTypeId();
    // Loop through each condition, if we find one that fails, then return FALSE.
    foreach($conditions as $condition) {
      $element = $token->replace($condition['element'], [ $entity_type => $entity ]);
      $value = $token->replace($condition['value'], [ $entity_type => $entity ] );
      $value1 = isset($condition['value1']) ? $token->replace($condition['value1'], [ $entity_type => $entity ] ) : '';
      $op = $condition['op'];
      // check the operation we are performing.
      switch ($op) {
        default:
          $condition_string = "return ({$element} {$op} {$value});";
        break;
        case 'range':
          $condition_string = "return ({$element} > {$value} && {$element} < {$value1});";
        break;
      }

      try {
        if (!eval($condition_string)) {
          return FALSE;
        }
      }
      catch (\Exception $e) {
        watchdog_exception('calculated_rate', $e);
        throw $e;
      }
    }
    // If all conditions pass, then return TRUE.
    return TRUE;
  }
}
